#include <iostream>

#include <oniguruma.h>

int main() {
    regex_t* reg;
    const UChar* pattern = (const UChar*)"a+b+$";
    const UChar* pattern_end = pattern + onigenc_str_bytelen_null(ONIG_ENCODING_UTF8, pattern);
    OnigErrorInfo einfo;

    onig_new(&reg, pattern, pattern_end, ONIG_OPTION_DEFAULT, ONIG_ENCODING_UTF8, ONIG_SYNTAX_DEFAULT, &einfo);


    onig_free(reg);

    return 0;
}
