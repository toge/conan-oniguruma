from conans import ConanFile, CMake, tools
import shutil

class OnigurumaConan(ConanFile):
    name        = "oniguruma"
    license     = "BSD"
    author      = "toge.mail@gmail.com"
    url         = "https://bitbucket.org/toge/conan-oniguruma/"
    homepage    = "https://github.com/kkos/oniguruma"
    description = "regular expression library "
    topics      = ("utf8", "regex")
    settings    = "os", "compiler", "build_type", "arch"
    generators  = "cmake"
    options = {
        "BUILD_SHARED_LIBS" : [True, False],
        "ENABLE_POSIX_API"  : [True, False],
    }
    default_options = {
        "BUILD_SHARED_LIBS" : False,
        "ENABLE_POSIX_API"  : False,
    }

    def source(self):
        tools.get(**self.conan_data["sources"][self.version])
        version = self.version.replace("_rev1", "")

        shutil.move("onig-{}".format(version), "onig")

    def build(self):
        cmake = CMake(self)
        cmake.definitions["BUILD_SHARED_LIBS"] = self.options.BUILD_SHARED_LIBS
        cmake.definitions["ENABLE_POSIX_API"] = self.options.ENABLE_POSIX_API
        cmake.configure(source_dir="onig")
        cmake.build()

    def package(self):
        self.copy("*.h",     dst="include", src="onig/src")
        self.copy("*.lib",   dst="lib",     keep_path=False)
        self.copy("*.dll",   dst="bin",     keep_path=False)
        self.copy("*.so",    dst="lib",     keep_path=False)
        self.copy("*.dylib", dst="lib",     keep_path=False)
        self.copy("*.a",     dst="lib",     keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["onig"]
